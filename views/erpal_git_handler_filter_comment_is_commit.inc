<?php

/**
 * @file
 * Definition of erpal_git_handler_filter_comment_is_commit.
 */

/**
 * Filter to handle filtering for many to one relationships.
 * Checks if comment has commit id.
 */
class erpal_git_handler_filter_comment_is_commit extends views_handler_filter_many_to_one {

  function get_value_options() {
    // Define options callback.
    $this->definition['options callback'] = '_erpal_git_comment_is_commit_options';
    parent::get_value_options();
  }

  /**
   * Clear query alter
   */
  function query() {
    // Prepare table.
    $this->ensure_my_table();
    $alias = $this->table_alias;
    
    // Distinct condition.
    $this->query->distinct = 1;

    // Check if not empty comment_is_commit value.
    if (!empty($this->view->filter['comment_is_commit']->value)) {
      if (is_array($this->view->filter['comment_is_commit']->value)) {
        
        $value = $this->view->filter['comment_is_commit']->value;

        // Skip if both 'Only commits' and 'Non commits' checked.
        if(in_array(1, $value) && in_array(2, $value)) {
          return;
        }

        // Join field_data_field_commit_id table.
        $join_project_ref = new views_join();
        $join_project_ref->construct('field_data_field_commit_id', $alias, 'cid', 'entity_id');
        $joined_table = $this->query->ensure_table('field_data_field_commit_id', $this->relationship, $join_project_ref);

        // Select only commits.
        if (in_array(1, $value)) {
          $expression = "{$joined_table}.field_commit_id_value IS NOT NULL";
        }

        // Select all comments except commits.
        elseif (in_array(2, $value)) {
          $expression = "{$joined_table}.field_commit_id_value IS NULL";
        }
        
        // Add where expression to query.
        $this->query->add_where_expression(0, $expression);
      }
    }
  }

}