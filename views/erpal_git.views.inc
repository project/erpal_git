<?php

/**
 * @file
 * Defines the various handler objects to help build and display views.
 */

/**
 * Implements of hook_views_data().
 */
function erpal_git_views_data() {
  return array(
    'comment' => array(
      'comment_is_commit' => array(
        'group' => t('Comment'),
        'title' => t('Comment is commit'),
        'help' => t('Checks if comment has commit id'),
        'filter' => array(
          'handler' => 'erpal_git_handler_filter_comment_is_commit',
        )
      ),
    ),
  );
}

/**
 * Views options callback
 * @return array
 *   Options array
 */
function _erpal_git_comment_is_commit_options() {
  $options = array(
    '1' => 'Only commits',
    '2' => 'Non commits',
  );

  return $options;
}
