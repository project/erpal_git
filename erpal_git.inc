<?php

/**
 * @file
 * Code for the erpal_git services module helper functions and callbackss.
 */

/**
 * Works with git commits
 * 
 * @param object $data
 *  Format of posted JSON Object:
 * 
 *  before: ID of the previous commit, may be empty if new branch set.
 *  after: ID of the new commit
 *  ref:	which Branch / Tag was updated, Master-Branch could be for example 'refs/heads/master'
 *  commits:	Array of commit-objects (see below)
 *    Format of commit objects
 *      id: ID (Hash) of the commit
 *      message: Commit-Message
 *      timestamp: Date and time of the Commit. Format yyyy-mm-dd'T'hh:mm:ssz
 *      url: URL to the commit in a repository viewer
 *      author: Object with author information and fields "name" and "email"
 *  repository:	Object with information about the repository
 *    Format of the repository object
 *      name:	Name of the repository
 *      url: GIT-URL
 */
function _erpal_git_commit($data) {
  if (!isset($data['payload']) || empty($data['payload'])) {
    return t('Can\'t find payload');
  }
  $payload = $data['payload'];
  $data = drupal_json_decode($payload);
  
  // Handle repository url.
  if (!isset($data['repository']['url']) || empty($data['repository']['url'])) {
    return t('Can\'t find repository url');
  }
  if(!$repository_url = preg_replace('/.*@/','',$data['repository']['url'])) {
    return t('Bad repository url');
  }
  
  // Handle commits array.
  if (!isset($data['commits']) || empty($data['commits']) || !is_array($data['commits'])) {
    return t('Can\'t find commits');
  }

  // Comment and update task using git commit message data.
  $result = array();
  
  // Sort commits descending. Because they are in vice versa order (from last to
  // first).
  $commits = $data['commits'];
  rsort($commits, SORT_NUMERIC);

  foreach ($commits as $commit) {
    if (isset($commit['id'])) {
      $result = array_merge($result, _erpal_git_commit_comment($commit, $repository_url));
    }
    else {
      $result = array('error' => t('Can\'t find commit Id'));
    }
  }
  return implode(". \n", $result);
}

/**
 * Comment node using git commit message data
 * Format of commit objects:
 *   id: ID (Hash) of the commit
 *   message:	Commit-Message
 *   timestamp:	Date and time of the Commit. Format yyyy-mm-dd'T'hh:mm:ssz
 *   url:	URL to the commit in a repository viewer
 *   author: Object with author information and fields "name" and "email"
 */
function _erpal_git_commit_comment($data, $repository_url) {
  $result = array();

  // Check if message exists.
  if (!isset($data['message'])) {
    return array('error' => t('Can\'t find message'));
  }
  else {
    $message = $data['message'];
  }

  // Check if nid exists.
  if (!preg_match('/\[#\d+\]/', $message, $matches)) {
    return array('error' => t('Can\'t find #nid tag in message'));
  }
  else {
    // Remove [#...] symbols.
    $_nid = str_replace('[#', '', $matches[0]);
    $nid = str_replace(']', '', $_nid);
  }

  // Check if node exists and node type is erpal_task.
  $node = node_load($nid);
  if (!$node || $node->type != 'erpal_task') {
    return array('error' => t('Can\'t find task #nid'));
  }
  
  // Get task project.
  if(!$project_id = field_get_items('node', $node, 'field_project_ref')) {
    return array('error' => t('Can\'t get project of task'));
  }
  if(empty($project_id[0]['target_id'])) {
    return array('error' => t('Can\'t get project of task'));
  }
  if(!$project = node_load($project_id[0]['target_id'])) {
    return array('error' => t('Can\'t get project of task'));
  }
  
  // Get repository url.
  if(!$repository = field_get_items('node', $project, 'field_repository_url')) {
    return array('error' => t('Can\'t get repository url'));
  }
  if(empty($repository[0]['value'])) {
    return array('error' => t('Can\'t get repository url'));
  }
  $project_repository_url = $repository[0]['value'];
  
  // If there is a username in the URL, cut it, and try againg.
  $repo_url_match = false;
  
  // Normalise the URLs to match without username.
  $repository_url_arr = explode('@', $repository_url);
  $project_repository_url_arr = explode('@', $project_repository_url);
  
  if (count($repository_url_arr) > 1) {
    $repository_url = $repository_url_arr[1];
  }
  
  if (count($project_repository_url_arr) > 1) {
    $project_repository_url = $project_repository_url_arr[1];
  }
  // Compare repo url.
  if($repository_url == $project_repository_url) {
    $repo_url_match = TRUE;
  }
  
  if (!$repo_url_match) {
    return array('error' => t('Url of you repository doesn\'t match project\'s repository url'));
  }
  
  // If the users email is not found in the system, the comment will be assigned 
  // to as admin.
  if (!isset($data['author']['email']) || !$user = user_load_by_mail($data['author']['email'])) {
    $user = user_load(1);
  }
  if (!node_access('update', $node, $user)) {
    return array('error' => t('!user doesn\'t have access to update task #!task', array(
        '!user' => $data['author']['email'],
        '!task' => $nid,
      )));
  }
  // Prepare comment body data.
  if(!$formatted_message = _erpal_git_prepare_message_data($data)) {
    return array('error' => t('Commit message without tags is empty.'));
  }

  // Comment subject.
  $subject = t('GIT commit');
  $commit_id = NULL;
  // Get commit ID.
  if (isset($data['id'])) {
    $subject .= ': ' . $data['id'];
    $commit_id = $data['id'];
  }

  //check if there is already a comment with this git commit message added
  $commit_exists = _erpal_git_commit_exists($commit_id);
  if ($commit_exists) {
    $result['status'] = array('warning' => t('Commit already exists.'));
    return $result;
  }
  
  // add comment
  $comment = _erpal_git_create_comment($nid, $user, $formatted_message, $subject, $commit_id);
  $result[] = t('Comment with commit message was added');

  // Change node status.
  if ($node_update_status = _erpal_git_change_task_status($node, $message)) {
    $result = array_merge($result, $node_update_status);
  }

  // Change assigned user.
  if ($node_update_user = _erpal_git_change_assigned_user($node, $message)) {
    $result = array_merge($result, $node_update_user);
  }
    
  // Change percent.
  if ($node_update_percent = _erpal_git_change_percent($node, $message)) {
    $result = array_merge($result, $node_update_percent);
  }

  // If at least one status was updates, save the node.
  if ($node_update_user || $node_update_status || $node_update_percent) {
    $node->revision = true; //create a new revision;
    $old_vid = $node->vid;

    // Do not create a new comment with nodechanges module,
    // because we want to do it with comment_alter module.
    if (module_exists('nodechanges') && module_exists('comment_alter')) {
      $node->nodechanges_skip = TRUE;
    }

    node_save($node);
    $new_vid = $node->vid;
    
    //integrate with comment_alter if enabled
    _erpal_git_comment_alter($old_vid, $new_vid, $comment);
    entity_get_controller('node')->resetCache(array($node->nid));  //clear cache
  }
   
  return $result;
}

/**
 * Creates comment
 * 
 * @param int $nid
 *   nid of a node you want to attach a comment to
 * @param object $user
 *   user who lefts the comment
 * @param string $message
 *   comment body
 * @param string $subject
 *   comment subject
 * 
 * @return object
 *   Created comment
 */
function _erpal_git_create_comment($nid, $user, $message, $subject = '', $commit_id = NULL, $pid = 0, $l = LANGUAGE_NONE) {
  
  $comment = new stdClass();
  $comment->nid = $nid; // nid of a node you want to attach a comment to
  $comment->cid = 0; // leave it as is
  $comment->pid = $pid; // parent comment id, 0 if none 
  $comment->uid = $user->uid; // user's id, who left the comment
  $comment->mail = $user->mail; // user's email
  $comment->name = $user->name;
  $comment->is_anonymous = 0; // leave it as is
  $comment->status = COMMENT_PUBLISHED; // We auto-publish this comment
  $comment->language = $l; // The same as for a node
  $comment->subject = $subject; // Comment subject
  $comment->comment_body[$l][0]['value'] = $message; // Comment body
  $comment->comment_body[$l][0]['format'] = 'filtered_html';

  if($commit_id) {
    $comment->field_commit_id[$l][0]['value'] = $commit_id; // Add Commit ID
  }
  
  comment_submit($comment); // saving a comment
  comment_save($comment);
  return $comment;
}

/**
* Checks with the git commit id if the commit was already added
* @param $commit_id the commit ID to check for existing comments
* @param $nid the nid if a node where to check for an attached comment with the given commit id
*/
function _erpal_git_commit_exists($commit_id, $nid=false) {
  //@TODO may add static cache
  $query = new EntityFieldQuery();

  $query->entityCondition('entity_type', 'comment')
    ->fieldCondition('field_commit_id', 'value', $commit_id)
    ->addMetaData('account', user_load(1)); // Run the query as user 1.
    
  if ($nid) {
    $query->propertyCondition('nid', $nid);
  }

  $result = $query->execute();

  if (isset($result['comment'])) {
    return array_keys($result['comment']);
  }
  
  return false;
  
}

/**
 * Prepare comment message body
 */
function _erpal_git_prepare_message_data($data) {
  if(!$clear_message = _erpal_git_clear_message($data['message'])) {
    return FALSE;
  }
  $message = array();
  // message:	Commit-Message
  if (isset($data['message'])) {
    $message['message'] = array(
      '#type' => 'item',
      '#markup' => $clear_message,
    );
  }
  // timestamp:	Date and time of the Commit. Format yyyy-mm-dd'T'hh:mm:ss
  /*if (isset($data['timestamp'])) {
    $message['timestamp'] = array(
      '#type' => 'item',
      '#markup' => t('Date') . ': ' . $data['timestamp']
    );
  }
  // url:	URL to the commit in a repository viewer
  if (isset($data['url'])) {
    $message['url'] = array(
      '#type' => 'item',
      '#markup' => t('Commit url') . ': ' . $data['url'],
    );
  }
  // Author
  $author = array();
  // author: "name"
  if (isset($data['author']['name']))
    $author[] = $data['author']['name'];
  // author: "email"
  if (isset($data['author']['email']))
    $author[] = $data['author']['email'];

  if (!empty($author)) {
    $message['email'] = array(
      '#type' => 'item',
      '#markup' => t('Author') . ': ' . implode(', ', $author),
    );
  }*/

  return render($message);
}

/**
 * Remove command information in comment text.
 */
function _erpal_git_clear_message($message) {
  $clear_message = preg_replace('/(\[#\d+\] ?)|(\[\?[\w ]+\] ?)|(\[\@[\w ,]+\] ?)|(\[\%\w+\] ?)/', '', $message);
  return $clear_message;
}

/**
* Function checks, if there is a commit message changing the percent field of a task
*/
function _erpal_git_change_percent($node, $message) {
  // Check if assigned user change tag exists.
  if (!preg_match('/\[\%\d+\]/', $message, $matches)) {
    return;
  }

  $_percent = str_replace('[%', '', $matches[0]);
  $percent = str_replace(']', '', $_percent);
  
  if (!$percent) {
    return array('error' => t('No percent value found.'));
  }
  
  $field = field_info_field('field_progress_percent');
  $allowed_percent_values = list_allowed_values($field);

  // Update node assigned user field.
  if ($percent && isset($allowed_percent_values[$percent])) {    
    $node->field_progress_percent[LANGUAGE_NONE][0]['value'] = $percent;
    return array('success' => t('Task percent value was updated to "!value"', array('!value' => $percent)));
  }
  
  return array('error' => t('No matching percent value found for tag "!value"', array('!value' => $percent)));
}


/**
  * Function checks, if there is a commit message assigning a new user to a task 
  * (not multiple supported yet)
  */
function _erpal_git_change_assigned_user($node, $message) {
  // Check if assigned user change tag exists.
  if (!preg_match('/\[\@[\w ,]+\]/', $message, $matches)) {
    return;
  }

  // Split users by comma.
  $_user = str_replace('[@', '', $matches[0]);
  $user_names_string = str_replace(']', '', $_user);
  $user_names = explode(',', $user_names_string);
  
  $assigned_users = array();
  
  $node->field_task_assigned_users = array();
  
  foreach ($user_names as $user_name) {
    $user_name = trim($user_name);
    
    // Check matches by user name.
    $all_users = _erpal_git_user_external_load($user_name);
  
    if (count($all_users) > 1) {
      return array('error' => t('There are more than one users that matches your tag. Use another @assigned tag.'));
    }

    $uid_keys = array_keys($all_users);
    $uid = reset($uid_keys);
    // Update node assigned user field.
    if ($uid) {    
      $node->field_task_assigned_users[LANGUAGE_NONE][]['target_id'] = $uid;
      $assigned_users[] = "{$user_name} ({$uid})";
    }
  }
  
  if(!empty($assigned_users)) {
    return array(
      'success' => t('Task assgined user was updated to "!names"', array(
        '!names' => implode(', ', $assigned_users),
      ))
    );
  }
  
  return array('error' => t('No matching user found for tag "!tag"', array('!tag' => $user_name)));
}

/**
  * Returns all users with username matching a given string.
  */
function _erpal_git_user_external_load($name = '') {
  $query = db_select('users', 'u')
    ->fields('u', array('name', 'uid'));
  
  if ($name) {
    $query->condition('name', '%'.$name.'%', 'LIKE');
  }

  $result = $query->execute();
  $users = array();
  foreach ($result as $row) {
    $users[$row->uid] = $row->name;
  }
  
  return $users;
}

/**
  * Function checks if there is a commit tag to change the status of a task.
  */
function _erpal_git_change_task_status($node, $message) {
  // Check if node status tag exists.
  if (!preg_match('/\[\?[\w ]+\]/', $message, $matches)) {
    return;
  }

  $_status = str_replace('[?', '', $matches[0]);
  $status = str_replace(']', '', $_status);
  if (!$vocabulary = taxonomy_vocabulary_machine_name_load('task_status_terms')) {
    return array('error' => t('Can\'t load vocabulary.'));
  }

  // We can use this function if condition for status tag is not "contains"
  // $terms = taxonomy_get_term_by_name($status, 'task_status_terms');
  // get status tid.
  $status_tid = '';
  $tree = taxonomy_get_tree($vocabulary->vid);
  foreach ($tree as $term) {
    // Check if term name contains the status.
    if (strpos(strtolower($term->name), strtolower($status)) !== FALSE) {
      // Check if only one term contains status tag.
      if (!$status_tid) {
        $status_tid = $term->tid;
      }
      else {
        // Break because only one term in vocabulary must contain status tag
        return array(
          'error' => t('There are more than one status in vocabulery that matches your tag. Use another ?status tag.'
        ));
      }
    }
  }

  // Update node status.
  if ($status_tid) {
    $node_status = field_get_items('node', $node, 'field_task_status_term');
    if ($node_status[0]['tid'] && $node_status[0]['tid'] != $status_tid) {
      $node->field_task_status_term[LANGUAGE_NONE][0]['tid'] = $status_tid;
      //node_save($node);  //node will be saved after all message tags have been processed
      return array('success' => t('Task status was updated'));
    } 
    else {
      return array('success' => t('Task already have this status'));
    }
  }
}

/**
 * Works with git commits after push.
 * Works only if 'payload' variable with JSON is in $_POST.
 * 
 * Format of posted JSON Object:
 *  before: ID of the previous commit, may be empty if new branch set.
 *  after: ID of the new commit
 *  ref:	which Branch / Tag was updated, Master-Branch could be for example 'refs/heads/master'
 *  commits:	Array of commit-objects (see below)
 *    Format of commit objects
 *      id: ID (Hash) of the commit
 *      message: Commit-Message
 *      timestamp: Date and time of the Commit. Format yyyy-mm-dd'T'hh:mm:ssz
 *      url: URL to the commit in a repository viewer
 *      author: Object with author information and fields "name" and "email"
 *  repository:	Object with information about the repository
 *    Format of the repository object
 *      name:	Name of the repository
 *      url: GIT-URL
 * 
 * @return string
 *   Information message about results of operation.
 */
function _erpal_git_push() {
  // TODO: do not superglobal variable $_POST directly. Use filters.  
  if (empty($_POST)) {
    //Compatibility mode according to https://github.com/gitlabhq/gitlabhq/issues/660
    $data = file_get_contents("php://input");
    $postdata['payload'] = $data;
  } else {
    $postdata = $_POST;
  }
    
  if(!empty($postdata['payload'])) {
    $data = array(
      'payload' => $postdata['payload'],
    );    
    watchdog('erpal_git_post', print_r($data,true));
    return _erpal_git_commit($data);
  }
}

/**
* Adds a comment alter dataset to show also the node changes in the comment
*/
function _erpal_git_comment_alter($old_vid, $new_vid, $comment) {
  if (module_exists('comment_alter')) {
    $comment_alter = array(
      'old_vid' => $old_vid,
      'new_vid' => $new_vid,
      'cid' => $comment->cid,
    );
    try {
      drupal_write_record('comment_alter', $comment_alter);
    } catch (Exception $e) {
    
    }
  }
}
