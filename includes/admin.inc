<?php

/**
 * @file
 * Code for the administration pages of the erpal_git module.
 */

/**
 * Form for generating security hash.
 */
function _erpal_git_generate_hash_form($form, $form_state) {
  $form = array();
  
  $form['user'] = array(
    '#type' => 'textfield',
    '#title' => t('User ID'),
    '#element_validate' => array('element_validate_integer_positive'),
    '#required' => TRUE,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Generate hash'),
  );
  
  return $form;
}

/**
 * Validion callback for generating security hash form.
 */
function _erpal_git_generate_hash_form_validate($form, $form_state) {
  if(!user_load($form_state['values']['user'])) {
    form_set_error('user', 'User with current ID doesn\'t exist');
  }
}

/**
 * Submit callback for generating security hash form.
 */
function _erpal_git_generate_hash_form_submit($form, $form_state) {
  $user = user_load($form_state['values']['user']);
  drupal_set_message(erpal_git_generate_hash($user));
}
